import { Component, OnInit } from '@angular/core';
import { DestinoViajesComponent } from '../destino-viajes/destino-viajes.component';
import {DestinoViaje} from '../models/destino-viaje.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.scss']
})
export class ListaDestinosComponent implements OnInit {
  destinos = [];
  constructor() {
  }

  ngOnInit(): void {
    //this.destinos = ['Cuba', 'Venezuela', 'Argentina', 'Dinamarca'];
  }

  guardar(nombre: string, url: string):boolean {
    this.destinos.push(new DestinoViaje(nombre, url));
    return false;
  }

}
